// Wow js

        new WOW().init();

// Ripple Effacts
		$(document).ready(function() {
			try {
				$('#hero,#feature,#screenshot,#pricing,#downloads,#subscribe,#contact').ripples({
					resolution: 512,
					dropRadius: 20, //px
					perturbance: 0.04,
				});
			}
			catch (e) {
				$('.error').show().text(e);
			}

			$('.js-ripples-disable').on('onmousemove', function() {
				$('#hero,#feature,#screenshot,#screenshot,#pricing,#downloads,#subscribe,#contact').ripples('destroy');
				$(this).hide();
			});

			$('.js-ripples-play').on('onmousemove', function() {
				$('#hero,#feature,#screenshot,#pricing,#downloads,#subscribe,#contact').ripples('play');
			});

			$('.js-ripples-pause').on('onmousemove', function() {
				$('#hero,#feature,#screenshot,#pricing,#downloads,#subscribe,#contact').ripples('pause');
			});

		});


// owl Carousel One


$('#owl-one').owlCarousel({
            animateOut: 'zoomOut',
   			animateIn: 'zoomIn',
		    loop:true,
		    autoplay:true,
			autoplayTimeout:2000,
			autoplayHoverPause:false,
		    margin:20,
		    nav:false,
		    dots:false,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
			});


// owl Carousel Two

$('#owl-two').owlCarousel({
    		animateOut: 'zoomOut',
   			animateIn: 'fadeInRight',
   			stagePadding:30,
    		smartSpeed:450,
        	loop:true,
        	autoplay:true,
        	dots:true,
        	margin:20,
		    nav:false,
		    items: 5,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:3
		        }
		    }
		});